import React from 'react';

// Components
import { Container, Error, Logo } from '../../components';

const NotFoundScreen = () => (
  <Container>
    <Logo />
    <Error />
  </Container>
);

export default NotFoundScreen;
