import styled from 'styled-components';
import { metrics } from '../../constants/metrics';

const Buttons = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  margin: 24px 0;
  flex-direction: column;

  @media ${metrics.tablet} {
    flex-direction: row;
  }
`;

export default Buttons;