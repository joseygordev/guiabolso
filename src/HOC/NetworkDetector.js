import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setConenction } from '../store/actions/connection';

// Components
import { ConnectionDown } from '../components';

export default function (ComposedComponent) {
  class NetworkDetector extends Component {
    state = {
      isDisconnected: false
    }

    componentDidMount() {
      this.handleConnectionChange();
      window.addEventListener('online', this.handleConnectionChange);
      window.addEventListener('offline', this.handleConnectionChange);
    }

    componentWillUnmount() {
      window.removeEventListener('online', this.handleConnectionChange);
      window.removeEventListener('offline', this.handleConnectionChange);
    }

    handleConnectionChange = () => {
      const { setConenction } = this.props;
      const condition = navigator.onLine ? 'online' : 'offline';
      
      if (condition === 'online') {
        const webPing = setInterval(
          () => {
            fetch('//google.com', {
              mode: 'no-cors',
            })
              .then(() => {
                setConenction(false);
                this.setState({ isDisconnected: false }, () => {
                  return clearInterval(webPing);
                });
              }).catch(() => {
                setConenction(true);
                this.setState({ isDisconnected: true });
              });
          }, 2000);
        return;
      }

      setConenction(true);
      return this.setState({ isDisconnected: true });
    }

    render() {
      const { isDisconnected } = this.state;
      return (
        <React.Fragment>
          { isDisconnected && <ConnectionDown /> }
          <ComposedComponent {...this.props} />
        </React.Fragment>
      );
    }
  }

  return connect(null, { setConenction })(NetworkDetector);
}
