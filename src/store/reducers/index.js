import { combineReducers } from 'redux';

import categories from './categories';
import connection from './connection';
import jokes from './jokes';

export default combineReducers({
  data: combineReducers({
    categories,
    connection,
    jokes
  })
});
