// Services
import { APIService } from '../../../services';

// Types
import { SAVE_CATEGORIES } from './types';

/**
 * Save categories on store
 */
export const saveCategories = (categories) => {
  return {
    type: SAVE_CATEGORIES,
    payload: categories
  };
};

export const getJokesCategories = () => {
  return async (dispatch) => {
    const { data } = await APIService.jokes.fetchAll();
    dispatch(saveCategories(data));
    return data;
  };
};
