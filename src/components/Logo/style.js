import styled from 'styled-components';

const Root = styled.img`
  width: 80%;
  margin: 5%;
  max-width: 400px;
`;

export default Root;