import React from 'react';

// Styles
import Root from './style';

const Logo = () => (
  <Root src="https://api.chucknorris.io/img/chucknorris_logo_coloured_small@2x.png" alt="Chuck Norris" />
);

export default Logo;
