import React from 'react';
import { withTheme } from 'styled-components';
import { BallBeat } from 'react-pure-loaders';
import PropTypes from 'prop-types';

const Loader = ({ theme }) => {
  return (
    <BallBeat color={theme.primary} loading />
  );
};

Loader.propTypes = {
  theme: PropTypes.shape({
    primary: PropTypes.string.isRequired
  }).isRequired,
};
Loader.defaultProps = {
};

export default withTheme(Loader);
