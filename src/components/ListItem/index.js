import React from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  ListButton,
  ListLine,
  ListTitle,
  ListContent
} from './style';

const ListItem = ({ item, onClick }) => {
  const renderContent = () => {
    return (
      <React.Fragment>
        {item.title && <ListTitle>{item.title}</ListTitle>}
        {item.content && <ListContent>{item.content}</ListContent>}
      </React.Fragment>
    );
  };

  const render = () => {
    if (onClick) {
      return (
        <ListButton type="button" onClick={() => onClick(item)}>
          {renderContent()}
        </ListButton>
      );
    } 
    return (
      <ListLine>
        {renderContent()}
      </ListLine>
    );
    
  };

  return render();
};

ListItem.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string,
    content: PropTypes.string,
  }),
  onClick: PropTypes.func,
};
ListItem.defaultProps = {
  item: {
    title: '',
    content: '',
  },
  onClick: () => {}
};

export default ListItem;
