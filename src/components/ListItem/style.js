import styled, { css } from 'styled-components';

const LineItem = css`
  background: white;
  border-radius: 0.5rem;
  box-shadow: 0.125rem 0.125rem 0.25rem rgba(0, 0, 0, 0.25);
  display: flex;
  justify-content: space-between;
  padding: 1rem;
  margin: 1rem auto;
  width: 100%;
  flex-direction: column;
  border-left: 1rem solid ${props => props.theme.secondary};
`;

export const ListLine = styled.div`
  ${LineItem}
`;

export const ListButton = styled.button`
  ${LineItem}
  :hover {
    opacity: .5;
    cursor: pointer;
  }
`;

export const ListTitle = styled.h2`
  font-weight: bold;
  font-size: 1.5rem;
  margin: 0px;
  text-transform: capitalize;
`;

export const ListContent = styled.p`
  font-size: 1.25rem;
  margin: 0px;
  text-transform: capitalize;
  margin: 16px 0px 0px;
  text-align: left;
`;