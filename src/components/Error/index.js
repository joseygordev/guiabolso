import React from 'react';

// Styles
import Root from './style';

const Error = () => {
  return (
    <Root>Something went wrong :( <br /> please try again</Root>
  );
};

export default Error;
