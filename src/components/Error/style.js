import styled from 'styled-components';

const Root = styled.h3`
  text-align: center;
  line-height: 1.6;
  color: #000;
`;

export default Root;