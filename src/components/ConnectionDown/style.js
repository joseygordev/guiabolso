import styled, { keyframes } from 'styled-components';

const slideOut = keyframes`
  0% {
    margin-top: 0;
  }
  100% {
    margin-top: -60px;
  }
`;

export const Root = styled.div`
  width: 100%;
  height: 60px;
  background-color: red;
  top: 0px;
  left: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  animation-name: ${slideOut};
  animation-iteration-count: once;
  animation-timing-function: ease-out;
  animation-duration: 1s;
  -webkit-animation-fill-mode: forwards;
  animation-fill-mode: forwards;
  animation-delay: 6s;
`;

export const Message = styled.p`
  font-size: 14px;
  color: #fff;
  text-align: center;
  font-weight: bold;
  padding: 12px;
`;
