import React from 'react';

// Styles
import { 
  Root,
  Message
} from './style';

const ConnectionDown = () => {
  return (
    <Root>
      <Message>You are disconnected, we are doing our best to keep your experience pleasant :)</Message>
    </Root>
  );
};

export default ConnectionDown;
