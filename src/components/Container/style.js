import styled from 'styled-components';

export const Root = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  max-width: 980px;
  width: 75%;
  height: 100vh;
  font-family: 'Open Sans', sans-serif;
  margin: auto;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  width: 100%;
  padding-bottom: 32px;
`;
