import React from 'react';
import PropTypes from 'prop-types';

// Styles
import { 
  Root,
  Wrapper
} from './style';

const Container = ({ children }) => (
  <Root>
    <Wrapper>
      {children}
    </Wrapper>
  </Root>
);

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};
Container.defaultProps = {
};

export default Container;
