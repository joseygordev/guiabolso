import React from 'react';
import PropTypes from 'prop-types';

// Styles
import Root from './style';

const Button = ({ label, onClick }) => (
  <Root type="button" onClick={onClick}>
    {label}
  </Root>
);

Button.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};
Button.defaultProps = {
};

export default Button;
