import styled from 'styled-components';
import { metrics } from '../../constants/metrics';

const Root = styled.button`
  font-weight: bold;
  font-size: 1rem;
  margin: 12px 0px;
  text-transform: uppercase;
  padding: 8px 24px;
  background: ${props => props.theme.secondary};
  border-radius: 5px;
  cursor: pointer;
  line-height: 1.4;
  color: #fff;
  width: 100%;

  @media ${metrics.tablet} {
    width: auto;
  }

  :hover {
    box-shadow: 0 0.125rem 0.25rem rgba(0,0,0,0.25);
    opacity: .75;
  }
`;

export default Root;