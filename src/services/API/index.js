import axios from 'axios';

const request = axios.create({
  baseURL: 'https://api.chucknorris.io'
});

// List of all base endpoints:
const ENDPOINTS = {
  JOKES: '/jokes',
};

/**
 * JOKES endpoint function calls.
 */
const jokes = {
  fetchAll: () => {
    return request.get(`${ENDPOINTS.JOKES  }/categories`);
  },

  getByCategory: (category) => {
    return request.get(`${ENDPOINTS.JOKES  }/random?category=${category}`);
  },
};

export default {
  jokes
};
