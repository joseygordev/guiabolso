import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';

import { store, persistor } from './store';
import { Jokes, Joke, NotFoundScreen } from './screens';
import reset from './constants/reset';
import theme from './constants/theme';

const GlobalStyle = createGlobalStyle`${reset}`;

ReactDOM.render(
  <Fragment>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Jokes} />
              <Route exact path="/joke/:category" component={Joke} />
              <Route component={NotFoundScreen} />
            </Switch>
          </BrowserRouter>
        </ThemeProvider>
      </PersistGate>
    </Provider>
    <GlobalStyle />
  </Fragment>,
  document.getElementById('root')
);
