
<h1 align="center">
<br>
  <a href="https://joseygordev@bitbucket.org/joseygordev/guiabolso.git"><img src="https://www.guiabolso.com.br/assets/images/logo-guiabolso.svg" alt="GuilaBolso" width=300"></a>
<br>

GuiaBolso Test

</h1>

## Introduction
This project is a test for GuiaBolso.

## Features

This project features.

- ⚛ **React** — 16.7.0-alpha.0 with Hooks
- ♻ **Redux with Redux Thunk** — State Management with middleware to handle async requests
- 🛠 **Babel** — ES6 syntax, Airbnb & React/Recommended config
- 🚀 **Webpack**  — Hot Reloading, Code Splitting, Optimized Build
- 💅 **CSS** — Styled Components
- ✅  **Tests** — Jest, React Testing Library & Cypress
- 💖  **Lint** — ESlint
- 🐶  **Husky** — Prevent bad commits

## Getting started

1. Clone this repo using `https://joseygordev@bitbucket.org/joseygordev/guiabolso.git`
2. Move to the appropriate directory: `cd guiabolso`.<br />
3. Run `yarn` or `npm install` to install dependencies.<br />
4. Run `yarn start` to see the example app at `http://localhost:8080`.

## Commands

- `yarn start` - start the dev server
- `yarn run build` - create a production ready build in `dist` folder
- `yarn run lint` - execute an eslint check
- `yarn run lint:fix` - execute an eslint and fix the errors
- `yarn test` - run all tests
- `yarn run test:watch` - run all tests in watch mode
- `yarn run test:cover` - coverage mode
- `yarn run cypress:open` - starts cypress
